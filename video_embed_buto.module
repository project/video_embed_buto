<?php

/**
 * @file
 * Adds a handler for Buto videos to Video Embed Field.
 *
 * @see video_embed_field.api.php for more documentation
 */

/**
 * Implements hook_video_embed_handler_info().
 */
function video_embed_buto_video_embed_handler_info() {
  $handlers['buto'] = array(
    'title' => 'Buto Video',
    'function' => 'video_embed_buto_handle_video',
    'thumbnail_function' => 'video_embed_buto_handle_thumbnail',
    'thumbnail_default' => drupal_get_path('module', 'video_embed_buto') . '/img/buto.png',
    'form' => 'video_embed_buto_form',
    'form_validate' => 'video_embed_buto_form_validate',
    'domains' => array(
      'buto.tv',
      'embed.buto.tv',
    ),
    'defaults' => array(
      'width' => 640,
      'height' => 360,
    ),
  );

  return $handlers;
}

/**
 * Defines the form elements for the Buto videos configuration form.
 *
 * @param array $defaults
 *   The form default values.
 *
 * @return array
 *   The provider settings form array.
 */
function video_embed_buto_form($defaults) {
  $form = array();

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Width'),
    '#description' => t('The width of the player.'),
    '#default_value' => $defaults['width'],
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Height'),
    '#description' => t('The height of the player.'),
    '#default_value' => $defaults['height'],
  );

  return $form;
}

/**
 * Validates the form elements for the Buto video configuration form.
 *
 * @param array $element
 *   The form element to validate.
 * @param array $form_state
 *   The form to validate state.
 * @param array $form
 *   The form to validate structure.
 */
function video_embed_buto_form_validate($element, &$form_state, $form) {
  video_embed_field_validate_dimensions($element);
}

/**
 * Handler for Buto videos.
 *
 * @param string $url
 *   The video URL.
 * @param array $settings
 *   The settings array.
 *
 * @return string|bool
 *   The video iframe, or FALSE in case the ID can't be retrieved from the URL.
 */
function video_embed_buto_handle_video($url, $settings) {
  $id = _video_embed_buto_get_video_id($url);

  if ($id) {
    // Our embed code.
    $embed='<iframe src="//embed.buto.tv/!id" width="@width" height="@height"></iframe> ';
    // Use format_string to replace our placeholders with the settings values.
    $embed = format_string($embed, array(
      '!id' => $id,
      '@width' => $settings['width'],
      '@height' => $settings['height'],
    ));

    $video = array(
      '#markup' => $embed,
    );
    return $video;
  }

  return FALSE;
}

/**
 * Gets the thumbnail url for Buto videos.
 *
 * @param string $url
 *   The video URL.
 *
 * @return array
 *   The video thumbnail information.
 */
function video_embed_buto_handle_thumbnail($url) {
  $id = _video_embed_buto_get_video_id($url);

  $html = drupal_http_request($url);

  if ($html->code == '200') {
    $html = $html->data;
  }
  else {
    return FALSE;
  }


  // Parsing html begins here.
  $doc = new DOMDocument();
  @$doc->loadHTML($html);
  $metas = $doc->getElementsByTagName('meta');

  for ($i = 0; $i < $metas->length; $i++) {
    $meta = $metas->item($i);
    if ($meta->getAttribute('property') == 'og:image') {
      $thumb_url = $meta->getAttribute('content');
    }
  }

  return array(
    'id' => $id,
    'url' => $thumb_url,
  );
}

/**
 * Helper function to get the Buto video's id.
 *
 * @param string $url
 *   The video URL.
 *
 * @return string
 *   The video ID.
 */
function _video_embed_buto_get_video_id($url) {
  $parts = parse_url($url);
  $id = substr($parts['path'], 1);
  return $id;
}
